<?
    $newsletter_url = Configure::read('NewsletterSignup.url');
    Router::connect( "$newsletter_url",   		 array( 'plugin' => 'NewsletterSignup', 'controller' => 'newsletter_signup', 'action' => 'index'   ) );
    Router::connect( "$newsletter_url/submit",   array( 'plugin' => 'NewsletterSignup', 'controller' => 'newsletter_signup', 'action' => 'index'   ) );
    Router::connect( "$newsletter_url/confirm",  array( 'plugin' => 'NewsletterSignup', 'controller' => 'newsletter_signup', 'action' => 'confirm' ) );