<?
    Router::connect( '/admin/newsletter_signup',             array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'NewsletterSignup', 'controller' => 'newsletter_signup', 'action' => 'index' ) );
    Router::connect( '/admin/newsletter_signup/:action/*',   array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'NewsletterSignup', 'controller' => 'newsletter_signup' ) );