<?php
    $this->set( 'title_for_layout', Configure::read('NewsletterSignup.title') );
    $this->params['url']['url'] = Configure::read('NewsletterSignup.url');
?>

<p>Thanks for signing up for our newsletter.</p>