<h1>Newsletter Sign-ups</h1>

<? if( sizeof( $newsletter_signups ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>
<? else: ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>

    <?= $html->link( "Export Sign-ups", '/admin/newsletter_signup/export', array( 'class' => 'button', 'escape' => false )  ); ?>

    <table class="list">
        <thead>
            <tr>
                <th><?= $paginator->sort('email');?></th>
                <th><?= $paginator->sort('created');?></th>
                <th class="t-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            <? foreach( $newsletter_signups AS $signup ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $signup['NewsletterSignup']['email'];   ?></td>
                    <td><?= $signup['NewsletterSignup']['created']; ?></td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $signup['NewsletterSignup']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $signup['NewsletterSignup']['email'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['NewsletterSignup']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>