<?php
    $this->set( 'title_for_layout', Configure::read('NewsletterSignup.title') );
    $this->params['url']['url'] = Configure::read('NewsletterSignup.url');
?>

<?= $this->element( 'form' ); ?>