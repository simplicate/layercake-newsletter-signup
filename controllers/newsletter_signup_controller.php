<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class NewsletterSignupController extends LayerCakeAppController {

	var $name       = 'NewsletterSignup';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'Xml' );
    var $uses       = array( 'NewsletterSignup.NewsletterSignup' );
    var $components = array( 'Session', 'Auth', 'Email', 'LayerCake.PersistentValidation', 'Content.PageContent' );

    function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'index', 'confirm' );
	}

    function index() {

        // if data was submitted and it validates
        if( !empty( $this->data ) ) {

            $this->NewsletterSignup->set( $this->data );
            if( $this->NewsletterSignup->validates() ) {

                // save to the database
                $this->NewsletterSignup->save();

                // redirect
                $this->redirect( Configure::read('NewsletterSignup.Url') . '/confirm' );

            } else {
                $this->redirect( Configure::read('NewsletterSignup.Url') );
            }
        }
    }

    function confirm() {
        $this->render();
    }


    // admin index
    function admin_index() {
        $this->paginate = array(
            'order' => array( 'NewsletterSignup.created DESC' ),
        );

		$this->NewsletterSignup->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/newsletter_signup/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
                    "NewsletterSignup.email LIKE" => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'newsletter_signups', $this->paginate( 'NewsletterSignup' ) );
	}


    // admin export
    function admin_export() {

        $this->NewsletterSignup->recursive = 1;

		// signups
        $signups = $this->NewsletterSignup->find( 'all', array( 'order' => array( 'NewsletterSignup.created DESC' ) ) );
        $this->set( 'records', $signups );

		Configure::write('debug', '0');
		$this->set( 'filename', 'Newsletter_Signups_' . date( 'Ymd_H-i' ) . '.csv' );
        $this->render( 'admin_export', 'csv' );
	}


	// admin - delete record
    function admin_delete( $id = null ) {

        // make sure the id has been set before we try to delete
        if( !$id ) {
			$this->Session->setFlash(__('Invalid ID for Record', true), 'default', array( 'class' => 'error' ) );
			$this->redirect( $this->referer() );
		}

		// do the delete
        if ($this->NewsletterSignup->delete($id)) {
			$this->Session->setFlash(__('Record deleted', true), 'default', array( 'class' => 'success' ) );
			$this->redirect( $this->referer() );
		}
    }
}