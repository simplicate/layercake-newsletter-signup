<? class NewsletterSignup extends AppModel {

	var $name = 'NewsletterSignup';
	var $validate = array(
        'email'         => array(
            'email' => array(
                'rule'    => 'email',
                'message' => 'Check you typed your email properly.'
            ),

            'notempty' => array(
                'rule'    => 'notempty',
                'message' => 'Please enter an email address.'
            ),

        ),
	);
}